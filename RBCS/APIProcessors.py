# -*- coding: utf-8 -*-

import json

import urllib
try: import urllib2
except:
	pass
# API processors
class APIProcessors(object):
	def __init__(self):
		# set up processor type
		self._Processor = {
			"http"      : {
				"tuling"    :   self._requestTuLing,
				"ars"       :   self._requestARS,
				},
			"database"  : {
				"None"      : self._requestDB
				}
		}

		# set the url corresponding the API name
		self._requestURL = {
			"tuling"            : "http://www.tuling123.com/openapi/api",
			"ars"               : "http://deepintell.iask.in:12913/ars.php"
			}

		# responseParser
		self._ResponseParser = {
			"tuling"            : self._tulingParser,
			"ars"               : self._arsParser
			}

	# 不同 API 的处理

	# http 请求的处理
	# tuling
	def _requestTuLing(self, name, attrs, req):
		""" handle the request to tuling """
		# build and send request
		try:
			url = self._requestURL[name]
			key = attrs['key'].encode('utf-8') if 'key' in attrs else "8190486186fc431f8a5edaad91fd02a5"
			userid = attrs['userid'] if 'userid' in attrs else 0
			params = {
				'key': key,
				'info': req.encode('utf-8'),
				'userid': userid
			}

			try:
				response = urllib.request.urlopen(url=url, data=urllib.urlencode(params))
			except:
				try:
					response = urllib2.urlopen(url=url, data=urllib.urlencode(params))
				except:
					raise "urllib module ERROR"
		except KeyError:
			raise "No found the URL of {0} ".format(name)
		except Exception, ex:
			raise ex

		# handle the response
		try: retV = self._ResponseParser[name](response)
		except KeyError:
			raise "No found the response parser of {0}".format(name)
		except Exception, ex:
			raise ex

		return retV

		# ars
	def _requestARS(self, name, attrs, req):
		""" handle the request to tuling """
		# build and send request
		try:
			url = self._requestURL[name]
			amount = int(attrs['amount']) if 'amount' in attrs else 1
			print url
			print amount
			# Processing coding
			query = urllib2.quote(req.encode('utf-8'))
			print query

			params = {
				'amount': amount,
				'query': query,
			}
			print params
			url += "?" + urllib.urlencode(params)
			print("url : ",url)
			try:
				response = urllib.request.urlopen(url=url)
			except:
				try:
					response = urllib2.urlopen(url=url)
				except:
					raise "urllib module ERROR"
		except KeyError:
			raise "No found the URL of {0} ".format(name)
		except Exception, ex:
			raise ex

		# handle the response
		try:
			retV = self._ResponseParser[name](response)
		except KeyError:
			raise "No found the response parser of {0}".format(name)
		except Exception, ex:
			raise ex

		return retV

	# database 请求的处理
	def _requestDB(self, name, attr, req):
		"""handle the request to database"""
		pass


	# http响应解析
	# tuling
	def _tulingParser(self, response):
		response = response.read()
		src = json.loads(response)
		code = src["code"]
		retV = ""
		if code == 100000:
			# simple text
			return src["text"]
		elif code == 200000:
			# data with url
			retV += src["text"] + "\n\t" + src["url"]
			return retV
		elif code == 302000:
			# news
			retV += src["text"] + "\n\n\t\t"
			lst = src["list"]
			for ele in lst:
				tmp = ""
				tmp += ele["article"] + "\n\t\t"
				tmp += ele["detailurl"] + "\n\n\t\t"
				retV += tmp
			return retV
		elif code == 308000:
			retV += src["text"] + "\n\n\t\t"
			lst = src["list"]
			for ele in lst:
				tmp = ""
				tmp += ele["name"] + "\n\t\t"
				tmp += ele["info"] + "\n\t\t"
				tmp += ele["detailurl"] + "\n\n\t\t"
				retV += tmp
			return retV
		else:
			return "= ="

		# tuling
	def _arsParser(self, response):
		response = response.read()
		src = json.loads(response)
		return src['cmnt']

	def __call__(self, APIType, APIName, APIExAttr, APIReq):
		try:
			return self._Processor[APIType][APIName](APIName, APIExAttr, APIReq)
		except KeyError:
			raise "No found the processor of ({0}:{1})".format(APIType, APIName)
		except Exception, ex:
			raise ex
